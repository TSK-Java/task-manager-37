package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ApplicationVersionRequest;
import ru.tsc.kirillov.tm.dto.response.ApplicationVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение версии программы.";
    }

    @Override
    public void execute() {
        System.out.println("[Версия]");
        System.out.println("Клиент: " + getPropertyService().getApplicationVersion());
        ApplicationVersionResponse response = geSystemEndpoint().getVersion(new ApplicationVersionRequest());
        System.out.println("Сервер: " + response.getVersion());
    }

}
