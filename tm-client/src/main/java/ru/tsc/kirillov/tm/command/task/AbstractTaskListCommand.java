package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskListCommand extends AbstractTaskCommand {

    protected void printTask(@Nullable final List<Task> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        int idx = 0;
        for(final Task task: tasks) {
            if (task == null) continue;
            System.out.println(++idx + ". " + task);
        }
    }

}
