package ru.tsc.kirillov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "project";

    public ProjectRepository(@NotNull final Connection connection) {
        super(Project.class, connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Project project = new Project();
        project.setId(rowSet.getString("id"));
        project.setCreated(rowSet.getTimestamp("created"));
        project.setName(rowSet.getString("name"));
        project.setDescription(rowSet.getString("description"));
        project.setStatus(Status.toStatus(rowSet.getString("status")));
        project.setUserId(rowSet.getString("user_id"));
        project.setDateBegin(rowSet.getTimestamp("date_begin"));
        project.setDateEnd(rowSet.getTimestamp("date_end"));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null) return new String[]{};
        @NotNull final List<String> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT id FROM %s WHERE user_id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(rowSet.getString(1));
        }
        return result.toArray(new String[]{});
    }

}
