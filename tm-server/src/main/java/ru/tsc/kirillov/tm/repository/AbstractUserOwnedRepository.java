package ru.tsc.kirillov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    private final Class<M> clazz;

    public AbstractUserOwnedRepository(@NotNull final Class<M> clazz, @NotNull final Connection connection) {
        super(connection);
        this.clazz = clazz;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, name, description, status, user_id, date_begin, date_end) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, javaDateToSql(model.getCreated()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().toString());
            statement.setString(6, model.getUserId());
            statement.setTimestamp(7, javaDateToSql(model.getDateBegin()));
            statement.setTimestamp(8, javaDateToSql(model.getDateEnd()));
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final java.util.Date dateBegin,
            @Nullable final java.util.Date dateEnd
    ) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? ORDER BY %s",
                getTableName(),
                getColumnSort(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        @NotNull final String sql = String.format(
                "SELECT id FROM %s WHERE user_id = ? AND id = ? LIMIT 1", 
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.next();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? AND id = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? LIMIT ?, 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ? AND id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M update(@NotNull final M model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET created = ?, name = ?, description = ?, status = ?, date_begin = ?, date_end = ? "
                + "WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, javaDateToSql(model.getCreated()));
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getStatus().toString());
            statement.setTimestamp(5, javaDateToSql(model.getDateBegin()));
            statement.setTimestamp(6, javaDateToSql(model.getDateEnd()));
            statement.setString(7, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        long count = 0;
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) count = rowSet.getLong(1);
        }

        return count;
    }

}
