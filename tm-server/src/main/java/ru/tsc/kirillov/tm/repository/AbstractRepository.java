package ru.tsc.kirillov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.comparator.CreatedComparator;
import ru.tsc.kirillov.tm.comparator.DateBeginComparator;
import ru.tsc.kirillov.tm.comparator.StatusComparator;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }
    
    @NotNull
    protected String getColumnSort(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet rowSet);

    @Nullable
    protected java.sql.Timestamp javaDateToSql(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new java.sql.Timestamp(date.getTime());
    }

    @Nullable
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull List<M> result = new ArrayList<>();
        models
                .stream()
                .forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s",
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s ORDER BY %s",
                getTableName(),
                getColumnSort(comparator)
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE id = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s LIMIT ?, 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection
                .stream()
                .forEach(this::remove);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    @SneakyThrows
    public long count() {
        long count = 0;
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            if (rowSet.next()) count = rowSet.getLong(1);
        }
        return count;
    }

}
