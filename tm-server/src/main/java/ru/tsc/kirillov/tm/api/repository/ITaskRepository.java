package ru.tsc.kirillov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    Task create(@Nullable String userId, @NotNull String name);

    @Nullable
    Task create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(String userId, String projectId);

    void removeAllByProjectList(@Nullable String userId, @Nullable String[] projects);

}
