package ru.tsc.kirillov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String TABLE_NAME = "user";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet rowSet) {
        @NotNull final User user = new User();
        user.setId(rowSet.getString("id"));
        user.setLogin(rowSet.getString("login"));
        user.setPasswordHash(rowSet.getString("password_hash"));
        user.setEmail(rowSet.getString("email"));
        user.setFirstName(rowSet.getString("firstName"));
        user.setLastName(rowSet.getString("lastName"));
        user.setMiddleName(rowSet.getString("middleName"));
        user.setRole(Role.toRole(rowSet.getString("role")));
        user.setLocked(rowSet.getBoolean("locked"));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password_hash, email, firstName, lastName, middleName, role, locked) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User update(@NotNull final User model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password_hash = ?, email = ?, firstName = ?, lastName = ?, middleName = ?, "
                        + "role = ?, locked = ? "
                        + "WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPasswordHash());
            statement.setString(3, model.getEmail());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setString(7, model.getRole().toString());
            statement.setBoolean(8, model.getLocked());
            statement.setString(9, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, email, firstName, lastName, middleName, role, locked "
                + "FROM %s WHERE login = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, email, firstName, lastName, middleName, role, locked "
                        + "FROM %s WHERE email = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
