package ru.tsc.kirillov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "task";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    public TaskRepository(@NotNull final Connection connection) {
        super(Task.class, connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, name, description, status, user_id, project_id, date_begin, date_end) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, javaDateToSql(task.getCreated()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.setTimestamp(8, javaDateToSql(task.getDateBegin()));
            statement.setTimestamp(9, javaDateToSql(task.getDateEnd()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Task task = new Task();
        task.setId(rowSet.getString("id"));
        task.setCreated(rowSet.getTimestamp("created"));
        task.setName(rowSet.getString("name"));
        task.setDescription(rowSet.getString("description"));
        task.setStatus(Status.toStatus(rowSet.getString("status")));
        task.setUserId(rowSet.getString("user_id"));
        task.setProjectId(rowSet.getString("project_id"));
        task.setDateBegin(rowSet.getTimestamp("date_begin"));
        task.setDateEnd(rowSet.getTimestamp("date_end"));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET created = ?, name = ?, description = ?, status = ?, project_id = ?, date_begin = ?, date_end = ? "
                        + "WHERE user_id = ? AND id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, javaDateToSql(model.getCreated()));
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getStatus().toString());
            statement.setString(5, model.getProjectId());
            statement.setTimestamp(6, javaDateToSql(model.getDateBegin()));
            statement.setTimestamp(7, javaDateToSql(model.getDateEnd()));
            statement.setString(8, model.getUserId());
            statement.setString(9, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end "
                        + "FROM %s WHERE user_id = ? AND project_id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE user_id = ? AND project_id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            statement.executeUpdate();
        }
    }

    @Override
    public void removeAllByProjectList(@Nullable final String userId, @Nullable final String[] projects) {
        Stream.of(projects)
                .filter(p -> p != null && !p.isEmpty())
                .forEach(p -> removeAllByProjectId(userId, p));
    }

}
